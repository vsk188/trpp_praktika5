from main import convert

def test_CtoK():
    assert convert(0, "Celsius","Kelvin") == 273.15

def test_KtoC():
    assert convert(0, "Kelvin", "Celsius") == -273.15

def test_FtoC():
    assert convert(0, "Fahrenheit", "Celsius") == -17.77777777777778

def test_error():
    assert convert(0, "Fahrenhit", "Celsius") == "Ошибка! Введена не верная шкала"