import sys
def convert(val, input, output):
    if input == "Kelvin":
        if output == "Celsius":
            return float(val)-273.15
        elif output == "Fahrenheit":
            return( float(val)-273.15)*1.8000+32
        else:
            return "Ошибка! Введена не верная шкала"
    elif input == "Celsius":
        if output == "Kelvin":
            return float(val)+273.15
        elif output == "Fahrenheit":
            return (float(val)*1.8000)+32
        else:
            return "Ошибка! Введена не верная шкала"
    elif input == "Fahrenheit":
        if output == "Celsius":
            return (float(val) - 32) * 5/9
        elif output == "Kalvin":
            return (float(val) - 32) * 5/9 + 273.15
        else:
            return "Ошибка! Введена не верная шкала"
    else:
        return "Ошибка! Введена не верная шкала"

if __name__ == '__main__':
    val = sys.argv[1]
    input = sys.argv[2]
    output = sys.argv[3]
    print(convert(val, input, output))
